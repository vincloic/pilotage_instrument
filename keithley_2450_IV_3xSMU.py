#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 15:06:03 2020

@author: vincent
"""

### TODO
# 

import sys
import pyvisa
import time

#########################################################
#                Fonction Boucle de mesure  SWEEP       #
#########################################################
def boucle_mesure_sweep(chemin):
    for Vbg in range(-500,1000,50):
        fichier = open(chemin + "__Vbg-" + str(Vbg) + "mV" + ".csw", 'w')
        fichier.write("Vbg \t Ibg \t Vgs \t Igs \t Vds \t Ids") 
        SMU_Vbg.write(":SOUR:VOLT " + str(Vbg) +  " E-03")
        SMU_Vbg.write(":OUTP1 ON")
        for Vgs in range(-500,1000,50):
            SMU_Vgs.write(":SOUR:VOLT " + str(Vgs) +  " E-03")
            SMU_Vgs.write(":OUTP1 ON")
            for Vds in range(-500,1000,50):
                SMU_Vds.write(":SOUR:VOLT " + str(Vds) +  " E-03")
                SMU_Vds.write(":OUTP1 ON")
                ### Timming
                time.sleep(0.01)
                print(" =================================================================================")
                print("| Vbg= " + str(Vbg)  + "mV | Vgs =" + str(Vgs)  + "mV | Vds =" + str(Vds)  + "mV")
                print(" ---------------------------------------------------------------------------------")
                Ibg = SMU_Vbg.query(":MEAS:CURR?").replace("\n","")
                Igs = SMU_Vgs.query(":MEAS:CURR?").replace("\n","")
                Ids = SMU_Vds.query(":MEAS:CURR?").replace("\n","")

                fichier.write(str(Vbg) + "\t"  + str(Ibg) + "\t"  + str(Vgs) + "\t"  + str(Igs) + "\t"  + str(Vds) + "\t"  + str(Ids)) 
                SMU_Vds.write(":OUTP1 OFF")
            SMU_Vgs.write(":OUTP1 OFF")
        SMU_Vbg.write(":OUTP1 OFF")
        fichier.close()
        
        
        
#########################################################
#                          MAIN                         #
#########################################################
   
####################################### CONFIG VISA

## TEnsion V grille/source
try:
    SMU_Vgs = pyvisa.ResourceManager().open_resource("USB0::0x05E6::0x2450::04432796::0::INSTR", timeout=10000)
except:
    print("> Erreur SMU Vgs non trouvé")
    sys.exit()
    
## TEnsion V drain/source
try:
    SMU_Vds = pyvisa.ResourceManager().open_resource("USB0::0x05E6::0x2450::04542602::0::INSTR", timeout=10000)
except:
    print("> Erreur SMU Vds non trouvé")
    sys.exit()

## TEnsion V back gate
try:
    SMU_Vbg = pyvisa.ResourceManager().open_resource("USB0::0x05E6::0x2450::04542602::0::INSTR", timeout=10000)
except:
    print("> Erreur SMU Vbg non trouvé")
    sys.exit()


print(SMU_Vgs.query("*IDN?"))
print(SMU_Vds.query("*IDN?"))
print(SMU_Vbg.query("*IDN?"))

####################################### CONFIG SMU

SMU_Vgs.write(":SOUR:FUNC:MODE VOLT")               # SMU en mode tension
SMU_Vgs.write(":DISP:CURR:DIG 5")                   # 5 digit de mesure de courant
SMU_Vgs.write(":TRIG:CONT ON")                      # trigger non continue

SMU_Vds.write(":SOUR:FUNC:MODE VOLT")               # SMU en mode tension
SMU_Vds.write(":DISP:CURR:DIG 5")                   # 5 digit de mesure de courant
SMU_Vds.write(":TRIG:CONT ON")                      # trigger non continue

SMU_Vbg.write(":SOUR:FUNC:MODE VOLT")               # SMU en mode tension
SMU_Vbg.write(":DISP:CURR:DIG 5")                   # 5 digit de mesure de courant
SMU_Vbg.write(":TRIG:CONT ON")                      # trigger 


chemin = "C:\\Users\\userlocal\\Documents\\2022\\2022-10-06-Loic-35lab__MPI-100um-220GHz\\Auto\\Transistor_cXlY"

boucle_mesure_sweep(chemin)

SMU_Vbg.close()
SMU_Vgs.close()
SMU_Vds.close()
