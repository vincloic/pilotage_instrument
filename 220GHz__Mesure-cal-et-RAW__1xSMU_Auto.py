#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 15:06:03 2020

@author: vincent
"""

### TODO
# 

import sys
import pyvisa
#import time
#import skrf

#########################################################
#                   Fonction MESURE VNA                 #
#########################################################
def mesure_VNA(tension,chemin):
    ###################################### MESURE VNA
    # Mesure en cal
    print("| Mesure Cal ON", end = "........")
    VNA.write(":SENS1:CORR:STATE ON")

    print("| Mesure Courant", end = ".......")
    courant_2p5_calon = float(SMU_2p5.query(":MEAS:CURR?"))
    courant_sweep_calon = float(SMU_sweep.query(":MEAS:CURR?"))
    print("[OK]")
    
    # ~ VNA.write("TRS;WFS;OS2P")
    # ~ try:
      # ~ data_on = VNA.read(encoding='latin9')
    # ~ except:
      # ~ data_on = 0
      # ~ print("> Error read")

    print("[OK]")

    # Mesure en non cal
    print("| Mesure Cal OFF", end=".......")
    VNA.write(":SENS1:CORR:STATE OFF")
    
    print("| Mesure Courant", end = ".......")
    courant_2p5_caloff = float(SMU_2p5.query(":MEAS:CURR?"))
    courant_sweep_calon = float(SMU_sweep.query(":MEAS:CURR?"))
    print("[OK]")
    
    # ~ VNA.write("TRS;WFS;OS2P")
    # ~ try:
      # ~ data_off = VNA.read(encoding='latin9')
    # ~ except:
      # ~ data_off = 0
      # ~ print("> Error read")


    #data_off = VNA.read_raw()
    print("[OK]")

    ###################################### SAUVEGARDE FICHIER S2P
    # Enregistrement des data en cal et non cal
    print("| Sauvegarde Fichier", end="...")
    # ~ data_on = data_on[11:]
    # ~ data_off = data_off[11:]

    # ~ f_don = open(chemin+ "__cal-on__V2p5-" + str(courant_2p5_calon) + "RFgain-" + str(tension) + "mV-" + str(courant_sweep_calon) + ".s2p", 'w')
    # ~ f_don.write(data_on)
    # ~ f_don.close()

    # ~ f_doff = open(chemin + "__cal-off__V2p5-" + str(courant_2p5_caloff) + "RFgain-" + str(tension) + "mV-" + str(courant_sweep_caloff) + ".s2p", 'w')
    # ~ f_doff.write(data_off)
    # ~ f_doff.close()
    print("[OK]")


#########################################################
#                Fonction Boucle de mesure              #
#########################################################
def boucle_mesure(debut,fin, pas, fichier):
    for i in range(debut,fin,pas):
        SMU_2p5.write(":SOUR:VOLT 2500 E-03")
        SMU_2p5.write(":OUTP1 ON")
##      
        
        
        SMU_sweep.write(":SOUR:VOLT " + str(i)  + "E-03")
        SMU_sweep.write(":OUTP1 ON")
##      loat(SMU.query(":MEAS:CURR?"))
        print(" ===============================================================")
        print("| Tension = " + str(i)  + "mV")
        print(" ---------------------------------------------------------------")
       
        mesure_VNA(i,fichier)
        SMU.write(":OUTP1 OFF")
        

#########################################################
#                          MAIN                         #
#########################################################
   
####################################### CONFIG VISA
try:
    VNA = pyvisa.ResourceManager().open_resource("USB::2907::65488::1741843::0::INSTR", timeout=10000)
except:
    print("> Erreur : VNA non trouvé")
    sys.exit()

try:
    SMU_sweep = pyvisa.ResourceManager().open_resource("USB0::0x05E6::0x2450::04542592::0::INSTR", timeout=10000)
except:
    print("> Erreur SMU non trouvé")
    sys.exit()
    
try:
    SMU_2p5 = pyvisa.ResourceManager().open_resource("USB0::0x05E6::0x2450::04542592::0::INSTR", timeout=10000)
except:
    print("> Erreur SMU non trouvé")
    sys.exit()


print(VNA.query("*IDN?"))
print(SMU_sweep.query("*IDN?"))
print(SMU_2p5.query("*IDN?"))

####################################### CONFIG VNA + format des S2P
VNA.write("LANG NATIVE")
VNA.write(":CALC1:FORM:S2P:PORT PORT14")        # Choix du port 1 et 4

VNA.write(":CALC1:PAR:COUN 4")                  # Affiche 4 courbe
VNA.write(":CALC1:PAR1:DEF S11")                # Affiche S11
VNA.write(":CALC1:PAR2:DEF S14")                # Affiche S14
VNA.write(":CALC1:PAR3:DEF S41")                # Affiche S41
VNA.write(":CALC1:PAR4:DEF S44")                # Affiche S44
VNA.write(":DISP:WIND:SPL R2C2")                # affiche 4 plot

VNA.write(":FORM:SNP:FREQ MHZ")                 # demande la frequence en MHz
VNA.write(":FORM:SNP:PAR REIM")                 # demande un reel et imaginaire
VNA.write(":FORM:DATA ASC")                     # en mode ASCII

SMU_sweep.write(":SOUR:FUNC:MODE VOLT")               # SMU en mode tension
SMU_sweep.write(":DISP:CURR:DIG 5")                   # 5 digit de mesure de courant
SMU_sweep.write(":TRIG:CONT ON")                      # trigger non continue

SMU_2p5.write(":SOUR:FUNC:MODE VOLT")               # SMU en mode tension
SMU_2p5.write(":DISP:CURR:DIG 5")                   # 5 digit de mesure de courant
SMU_2p5.write(":TRIG:CONT ON")                      # trigger non continue

# Doucle de meusres debut, fin et pas, chemin de sauvegarde
# fichier = "Z:\\David\\version-4\\Vctrl\\etat2\\Mesure__version4_etat4__V6_1p6V__"
chemin = "Z:\\CIME\\Varactor-5\\Mesure__varactor-5__"

## Tension en mV
boucle_mesure(0, 3300, 300, chemin)

##boucle_mesure(1200,    100, -100, fichier)
##boucle_mesure( 100,   -100,  -50, fichier)
##boucle_mesure(-100,  -1300, -100, fichier)

VNA.close()
SMU_sweep.close()
SMU_2p5.close()
