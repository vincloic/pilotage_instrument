import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import time

import pyvisa

import sys

#### TO DO
#
# - Compliance des SMU
# - Enregistrement du fichier de mesure en cal OFF



class VNA_Auto(tk.Tk) :
    
    SMU_nbre = 0;
    Fichier_courant = ""
    SMU_VISA_list = []
    Mesure_possible = True
    
    def __init__(self, master=None):
        #super().__init__(master)
        tk.Tk.__init__(self,master)
        self.master = master
        self.Variable_init()
        self.GUI_init()
        self.VISA_list_update()
        #self.grid()
        
    def Variable_init(self):
        self.cb_S2Pcal = tk.BooleanVar()
        self.cb_S2Praw = tk.BooleanVar()    
        
        
############################################### VISA Liste resource
    def VISA_list_update(self):
        try :
            self.VISA = pyvisa.ResourceManager()
            self.VISA_list = self.VISA.list_resources()
        except :
            tk.messagebox.showwarning("VISA Error", "VISA not available")
            return
        if self.VISA_list == ():
            tk.messagebox.showwarning("VISA Error", "NO instrument available")
            sys.exit()
        
        self.SMUAdress_combo["values"] = self.VISA_list
        self.SMUAdress_combo.current(0)     
        self.VNAAdress_combo["values"] = self.VISA_list
        self.VNAAdress_combo.current(tk.END)
        

############################################### VNA VISA opne
    def VNA_VISA_open(self):
        try:
            self.consol_text.insert("end","| VNA open VISA ... ")
            # ~ self.VNA = pyvisa.ResourceManager().open_resource("USB::2907::65488::1741843::0::INSTR", timeout=10000)
            self.VNA = pyvisa.ResourceManager().open_resource(self.VNAAdress_combo.get(), timeout=60000)
            self.consol_text.insert("end","[OK]\n")
            self.consol_text.insert("end","| " + self.VNA.query("*IDN?"))
            self.consol_text.insert("end","|\n")
            self.Mesure_possible = True
        except:
            tk.messagebox.showwarning("VISA Error", "VNA not available")
            self.consol_text.insert("end","[FAIL]\n")
            self.consol_text.insert("end","|\n")
            self.Mesure_possible = False
            return
        
        
  ############################################### VNA VISA close
    def VNA_VISA_close(self):
        self.consol_text.insert("end","| VNA close VISA ... ")
        try :
            self.VNA.close()
            self.consol_text.insert("end","[OK]\n")
        except :
            self.consol_text.insert("end","[FAIL]\n")
            self.consol_text.insert("end","|\n")
              
############################################### VNA config
    def VNA_config(self) :
        self.consol_text.insert("end","| VNA config ... ")
         
        self.VNA.write("LANG NATIVE")
        self.VNA.write(":CALC1:FORM:S2P:PORT PORT14")        # Choix du port 1 et 4

        self.VNA.write(":CALC1:PAR:COUN 4")                  # Affiche 4 courbe
        self.VNA.write(":CALC1:PAR1:DEF S11")                # Affiche S11
        self.VNA.write(":CALC1:PAR2:DEF S14")                # Affiche S14
        self.VNA.write(":CALC1:PAR3:DEF S41")                # Affiche S41
        self.VNA.write(":CALC1:PAR4:DEF S44")                # Affiche S44
        self.VNA.write(":DISP:WIND:SPL R2C2")                # affiche 4 plot

        self.VNA.write(":FORM:SNP:FREQ MHZ")                 # demande la frequence en MHz
        self.VNA.write(":FORM:SNP:PAR REIM")                 # demande un reel et imaginaire
        self.VNA.write(":FORM:DATA ASC")                     # en mode ASCII
        self.consol_text.insert("end","[OK]\n")
        self.consol_text.insert("end","|\n")
        
############################################### SMU VISA
    def SMU_VISA_open(self, SMU_nbre):
        if SMU_nbre <= 0 :
            return
        else :
            self.consol_text.insert("end","| SMU open ... ")
            value = self.SMU_Table.item(SMU_nbre-1,'value')
            try:
                instru = pyvisa.ResourceManager().open_resource(value[1], timeout=60000)
                self.consol_text.insert("end","[OK]\n")
                self.consol_text.insert("end","| " + instru.query("*IDN?"))
                self.consol_text.insert("end","|\n")
                self.SMU_VISA_list.insert(0,instru)
                self.Mesure_possible = True
            except:
                tk.messagebox.showwarning("VISA Error", "SMU not available " + value[1])
                self.consol_text.insert("end","[FAIL]\n")
                self.consol_text.insert("end","|\n")
                self.Mesure_possible = False
        self.SMU_VISA_open(SMU_nbre - 1)
        

  ############################################### VNA VISA close
    def SMU_VISA_close(self):
        SMU_VISA_list_temp = self.SMU_VISA_list.copy()
        self.consol_text.insert("end","| SMU close VISA ... ")
        for inst in SMU_VISA_list_temp :
            try : 
                inst.close()
                self.SMU_VISA_list.remove(inst)
                self.consol_text.insert("end","[OK]\n")
                self.consol_text.insert("end","|\n")
            except :
                self.consol_text.insert("end","[FAIL]\n")
                self.consol_text.insert("end","|\n")
        
############################################### SMU config
    def SMU_config(self) :
        self.consol_text.insert("end","| SMU config ... ")
        for inst in range(0, len(self.SMU_VISA_list),1) :
                value = self.SMU_Table.item(inst,'value')
                if value[5] == "V"  :
                    self.SMU_VISA_list[inst].write(":SOUR:FUNC:MODE VOLT")               # SMU en mode tension
                else :
                    self.SMU_VISA_list[inst].write(":SOUR:FUNC:MODE CURR")               # SMU en mode tension
                self.SMU_VISA_list[inst].write(":DISP:CURR:DIG 5")                   # 5 digit de mesure de courant
                self.SMU_VISA_list[inst].write(":TRIG:CONT OFF")                      # trigger non continue
        self.consol_text.insert("end","[OK]\n")
        self.consol_text.insert("end","|\n")

        
############################################################################################## 
############################################### GRESTION SMU
############################################################################################## 

############################################### Select SMU
    def b_select_SMU(self,event=None):
        #clear entry boxes
        self.SMUNom_entry.delete(0,tk.END)
        self.SMUMin_entry.delete(0,tk.END)
        self.SMUMax_entry.delete(0,tk.END)
        self.SMUPas_entry.delete(0,tk.END)
        self.SMUCompl_entry.delete(0,tk.END)
        
        #grab record
        selected=self.SMU_Table.focus()
        #grab record values
        values = self.SMU_Table.item(selected,'values')
    
        #output to entry boxes
        self.SMUNom_entry.insert(0,values[0])
        self.SMUAdress_combo.set(values[1])
        self.SMUMin_entry.insert(0,values[2])
        self.SMUMax_entry.insert(0,values[3])
        self.SMUPas_entry.insert(0,values[4])
        self.SMUFunction_combo.set(values[5])
        self.SMUCompl_entry.insert(0,values[6])

    def isint(self,x):
        try:
            a = float(x)
            b = int(a)
        except (TypeError, ValueError):
            return False
        else:
            return True
############################################### save SMU
    def b_update_SMU(self):
        selected=self.SMU_Table.focus()
        #save new data 
        if self.SMUNom_entry.get() == '' or self.SMUMin_entry.get() == '' or self.SMUMax_entry.get() == '' or self.SMUPas_entry.get() == ''or self.SMUCompl_entry.get() == '' : 
            tk.messagebox.showwarning("Erreur donnée", "Attention donnée vide")
        if self.isint(self.SMUMin_entry.get()) == False or self.isint(self.SMUMax_entry.get()) == False or self.isint(self.SMUPas_entry.get()) == False or self.isint(self.SMUCompl_entry.get()) == False : 
            tk.messagebox.showwarning("Erreur donnée", "Attention donnée non entière")
        else :
            self.SMU_Table.item(selected,text="",values=(self.SMUNom_entry.get(), self.SMUAdress_combo.get(),self.SMUMin_entry.get(),self.SMUMax_entry.get(),self.SMUPas_entry.get(), self.SMUFunction_combo.get(), self.SMUCompl_entry.get()))
               
        #clear entry boxes
        self.SMUNom_entry.delete(0,tk.END)
        self.SMUMin_entry.delete(0,tk.END)
        self.SMUMax_entry.delete(0,tk.END)
        self.SMUPas_entry.delete(0,tk.END)
        self.SMUCompl_entry.delete(0,tk.END)
    
############################################### Add SMU
    def b_add_SMU(self):
        #save new data 
        if self.SMUNom_entry.get() == '' or self.SMUMin_entry.get() == '' or self.SMUMax_entry.get() == '' or self.SMUPas_entry.get() == ''or self.SMUCompl_entry.get() == '' : 
            tk.messagebox.showwarning("Erreur donnée", "Attention donnée vide")
        if self.isint(self.SMUMin_entry.get()) == False or self.isint(self.SMUMax_entry.get()) == False or self.isint(self.SMUPas_entry.get()) == False : 
            tk.messagebox.showwarning("Erreur donnée", "Attention donnée non entière")
        else :
            self.SMU_Table.insert(parent='',index='end',text='',iid=self.SMU_nbre, values=(self.SMUNom_entry.get(), self.SMUAdress_combo.get(),self.SMUMin_entry.get(),self.SMUMax_entry.get(),self.SMUPas_entry.get(), self.SMUFunction_combo.get(), self.SMUCompl_entry.get()))
            self.SMU_nbre += 1
        self.consol_text.insert("1.0",self.SMU_nbre)
        #clear entry boxes
        self.SMUNom_entry.delete(0,tk.END)
        self.SMUMin_entry.delete(0,tk.END)
        self.SMUMax_entry.delete(0,tk.END)
        self.SMUPas_entry.delete(0,tk.END)
        self.SMUCompl_entry.delete(0,tk.END)
               
############################################### Delete SMU
    def b_del_SMU(self):
        selected=self.SMU_Table.focus()
        self.SMU_nbre -= 1
        self.SMU_Table.delete(selected)
        
############################################################################################## 
############################################### MESURE
############################################################################################## 
    def b_Mesure(self):
        if tk.messagebox.askokcancel(title="Mesure ?", message="Voulez vous lancer les mesures ?") :
            self.consol_text.insert("end", " =============================================\n")
            self.consol_text.insert("end", "               Début Mesure \n")
            self.consol_text.insert("end", "   " + time.ctime() + "\n")
            self.consol_text.insert("end", " =============================================\n")
            self.consol_text.see("end")
            self.Mesure_cours_label.config(state = "normal")
            
            self.Fichier_courant = self.Fichier_Nom_entry.get()
            
            self.SMU_VISA_open(self.SMU_nbre)
            if self.Mesure_possible :
                self.SMU_config()
            
            if self.Mesure_possible :
                self.VNA_VISA_open()
            
            if self.Mesure_possible :
                self.VNA_config()
            
            if self.Mesure_possible :  
                self.Boucle_mesure(self.SMU_nbre, self.SMU_nbre)
            
            self.VNA_VISA_close()
            self.SMU_VISA_close()
            
            self.consol_text.insert("end", " =============================================\n")
            self.consol_text.insert("end", "                 Fin Mesure \n")
            self.consol_text.insert("end", "   " + time.ctime() + "\n")
            self.consol_text.insert("end", " =============================================\n")
            self.consol_text.see("end")

            self.Mesure_cours_label.config(state = "disabled")

###############################################  Mesure VNA
    def Boucle_mesure_VNA (self) :
        if self.cb_S2Pcal.get(): 
            self.consol_text.insert("end", "| " + time.ctime() + "\n")
            self.consol_text.insert("end", "| Mesure s2p CAL ON ...")
            VNA.write(":SENS1:CORR:STATE ON")
            self.VNA.write("TRS;WFS;OS2P")
            try:
                data_on = self.VNA.read(encoding='latin9')
                self.consol_text.insert("end","[OK] \n")
                data_on = data_on[11:]
                f_don = open(self.Fichier_Chemin_entry.get() + self.Fichier_courant +".s2p", 'w')
                f_don.write(data_on)
                f_don.close()
            except :
                self.consol_text.insert("end","[ERR READ] \n")
            # ~ print(self.VNAAdress_combo.get() + "TRS;WFS;OS2P")
            # ~ print("Sauvegarde cal : " + self.Fichier_courant)

        if self.cb_S2Praw.get() :
            self.consol_text.insert("end", "| " + time.ctime() + "\n")
            self.consol_text.insert("end"," | Mesure s2p CAL OFF ...\n")
            # ~ print(self.VNAAdress_combo.get() + "TRS;WFS;OS2P")
            # ~ print("Sauvegarde raw : " + self.Fichier_courant)
            self.consol_text.insert("end","[OK] \n")
        self.consol_text.insert("end", " ---------------------------------------------\n")
        self.consol_text.see("end")

###############################################  Boucle mesures des donnes des SMU
    def Boucle_mesure_SMU(self, SMU_nbre):
        if SMU_nbre <= 0 :
            return
        else:
            value = self.SMU_Table.item(SMU_nbre-1,'value')
            if value[5] == 'V' :
                fichier_temp = self.Fichier_courant
                # ~ print(value[1] + ":MEAS:CURR?" + str(i*1.2))
                courant = self.SMU_VISA_list[SMU_nbre-1].query(":MEAS:CURR?")
                self.consol_text.insert("end","| " + value[0] + " " + courant.replace("\n","")  + "A\n")
                self.Fichier_courant = self.Fichier_courant.replace("^" + value[0]+ "^", courant.replace("-","m").replace(".","p").replace("\n",""))
            else :
                fichier_temp = self.Fichier_courant
                tension = self.SMU_VISA_list[SMU_nbre-1].query(":MEAS:CURR?")
                self.consol_text.insert("end","| " + value[0] + " " + tension.replace("\n","")  + "V\n")
                self.Fichier_courant = self.Fichier_courant.replace("^" + value[0]+ "^", tension.replace("-","m").replace(".","p").replace("\n",""))
            self.consol_text.see("end")
            self.Boucle_mesure_SMU(SMU_nbre - 1)
            #self.Fichier_courant = fichier_temp

###############################################  Appliquer les valeurs eu Source V et/ou I
    def Boucle_mesure(self, SMU_nbre, SMU_nbre_max):
        if SMU_nbre <= 0 :
            self.Boucle_mesure_SMU(SMU_nbre_max)
            self.Boucle_mesure_VNA()
            return
        else:
            value = self.SMU_Table.item(SMU_nbre-1,'value')
            for i in range(int(value[2]), int(value[3])+1, int(value[4])):
                if value[5] == 'V' :
                    fichier_temp = self.Fichier_courant
                    self.consol_text.insert("end", "| " + value[0] + " " + str(i)  + "mV\n")
                    self.SMU_VISA_list[SMU_nbre-1].write(":SOUR:VOLT " + str(i)  + "E-03")
                    self.SMU_VISA_list[SMU_nbre-1].write(":OUTP1 ON")
                    # ~ self.SMU_VISA_list[SMU_nbre-1].write(":TRIG:CONT REST")
                    # ~ print(value[0] + ":SOUR:VOLT " + str(i)  + "E-03")
                    # ~ print(self.SMU_VISA_list[SMU_nbre-1])#.query("*IDN?*"))
                    self.Fichier_courant += "___" + value[0] +  "-" + str(i).replace("-","m").replace(".","p")  + "mV--^" + value[0] +  "^A"
                else :
                    fichier_temp = self.Fichier_courant
                    self.consol_text.insert("end", "| " + value[0] + " " + str(i)  + "mA \n")
                    self.SMU_VISA_list[SMU_nbre-1].write(":SOUR:CURR " + str(i)  + "E-03")
                    self.SMU_VISA_list[SMU_nbre-1].write(":OUTP1 ON")
                    # ~ self.SMU_VISA_list[SMU_nbre-1].write(":TRIG:CONT REST")
                    # ~ print(value[0] + ":SOUR:CURR " + str(i)  + "E-03")
                    self.Fichier_courant += "___" + value[0] + "-" + str(i).replace("-","m").replace(".","p")  + "-mA--^" + value[0] +  "^V"
                self.consol_text.see("end")
                self.Boucle_mesure(SMU_nbre - 1, SMU_nbre_max)
                self.SMU_VISA_list[SMU_nbre-1].write(":OUTP1 OFF")
                self.Fichier_courant = fichier_temp

############################################################################################## 
############################################### QUIT
############################################################################################## 
    def b_quit(self):
        if tk.messagebox.askokcancel(title="Quitter ?", message="Voulez vous quitter ? ") :
            sys.exit()
            
############################################################################################## 
############################################### GUI INIT
############################################################################################## 
    def GUI_init(self):
        
        self.frame_tab = tk.Frame(self)
        self.frame_tab.pack() #grid() #pack()
        
        
        ################ TABLEAU
        #scrollbar
        self.gui_scroll = tk.Scrollbar(self.frame_tab)
        self.gui_scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.gui_scroll = tk.Scrollbar(self.frame_tab,orient='horizontal')
        self.gui_scroll.pack(side= tk.BOTTOM,fill=tk.X)
        
        self.SMU_Table = ttk.Treeview(self.frame_tab,yscrollcommand=self.gui_scroll.set, xscrollcommand =self.gui_scroll.set,selectmode='browse')
        self.SMU_Table.bind("<<TreeviewSelect>>", self.b_select_SMU)
        self.SMU_Table.pack()
        
        self.gui_scroll.config(command=self.SMU_Table.yview)
        self.gui_scroll.config(command=self.SMU_Table.xview)
        
        #define our column
         
        self.SMU_Table['columns'] = ('SMU_Name', 'SMU_Adress', 'SMU_Min', 'SMU_Max', 'SMU_Pas', 'SMU_Function', 'SMU_Compliance')
        
        # format our column
        self.SMU_Table.column("#0", width=0,  stretch=tk.NO)
        self.SMU_Table.column("SMU_Name",anchor=tk.CENTER, width=100)
        self.SMU_Table.column("SMU_Adress",anchor=tk.CENTER, width=300)
        self.SMU_Table.column("SMU_Min",anchor=tk.CENTER,width=100)
        self.SMU_Table.column("SMU_Max",anchor=tk.CENTER,width=100)
        self.SMU_Table.column("SMU_Pas",anchor=tk.CENTER,width=100)
        self.SMU_Table.column("SMU_Function",anchor=tk.CENTER,width=100)
        self.SMU_Table.column("SMU_Compliance",anchor=tk.CENTER,width=100)
        
        
        #Create Headings 
        self.SMU_Table.heading("#0",text="",anchor=tk.CENTER)
        self.SMU_Table.heading("SMU_Name",text="Adresse",anchor=tk.CENTER)
        self.SMU_Table.heading("SMU_Adress",text="Adresse",anchor=tk.CENTER)
        self.SMU_Table.heading("SMU_Min",text="Start",anchor=tk.CENTER)
        self.SMU_Table.heading("SMU_Max",text="1.0",anchor=tk.CENTER)
        self.SMU_Table.heading("SMU_Pas",text="Step",anchor=tk.CENTER)
        self.SMU_Table.heading("SMU_Function",text="Function",anchor=tk.CENTER)
        self.SMU_Table.heading("SMU_Compliance",text="Compliance",anchor=tk.CENTER)
        
        
        ################ DATA DEBUG
        ################ DATA DEBUG
        ################ DATA DEBUG
        # ~ self.SMU_Table.insert(parent='',index='end',iid=self.SMU_nbre,text='',
        # ~ values=('SMU1', 'USB0::1510::9296::04542602::0::INSTR','0','10','4','V','10'))
        # ~ self.SMU_nbre += 1
        
        self.SMU_Table.insert(parent='',index='end',iid=self.SMU_nbre,text='',
        values=('SMU2', 'USB0::1510::9296::04542603::0::INSTR','11','20','4','V','10'))
        self.SMU_nbre += 1
        
        # ~ self.SMU_Table.insert(parent='',index='end',iid=self.SMU_nbre,text='',
        # ~ values=('SMU3', 'USB:///3','21','21','1','V','10'))
        # ~ self.SMU_nbre += 1
        
        self.SMU_Table.pack()
        
        self.frame_ctrl = tk.Frame(self)
        self.frame_ctrl.pack() #grid(row=0,column=0, pady=20)
        
        ################ LABEL VALEUR       
        self.SMUNom = tk.Label(self.frame_ctrl,text="Nom")
        self.SMUNom.grid(row=1,column=0)
        
        self.SMUMin = tk.Label(self.frame_ctrl,text="Start (mU)")
        self.SMUMin.grid(row=1,column=2)
        
        self.SMUMax = tk.Label(self.frame_ctrl,text="End (mU)")
        self.SMUMax.grid(row=1,column=3)
        
        self.SMUPas = tk.Label(self.frame_ctrl,text="Step (mU)")
        self.SMUPas.grid(row=1,column=4)
        
        self.SMUPas = tk.Label(self.frame_ctrl,text="Function")
        self.SMUPas.grid(row=1,column=5)
        
        self.SMUCompl = tk.Label(self.frame_ctrl,text="Compliance")
        self.SMUCompl.grid(row=1,column=6)
        
        ################ ENTRY BOX VALEUR         
        self.SMUNom_entry = tk.Entry(self.frame_ctrl)
        self.SMUNom_entry.grid(row=2,column=0)
        
        self.SMUAdress_combo = ttk.Combobox(self.frame_ctrl, state='readonly')
        self.SMUAdress_combo.grid(row=2, column = 1)
                
        self.SMUMin_entry = tk.Entry(self.frame_ctrl)
        self.SMUMin_entry.grid(row=2,column=2)
        
        self.SMUMax_entry = tk.Entry(self.frame_ctrl)
        self.SMUMax_entry.grid(row=2,column=3)
        
        self.SMUPas_entry = tk.Entry(self.frame_ctrl)
        self.SMUPas_entry.grid(row=2,column=4)
        
        self.SMUFunction_combo = ttk.Combobox(self.frame_ctrl, values=['V', 'I'], state='readonly')
        self.SMUFunction_combo.grid(row=2, column = 5)
        self.SMUFunction_combo.current(0)
        
        self.SMUCompl_entry = tk.Entry(self.frame_ctrl)
        self.SMUCompl_entry.grid(row=2,column=6)
        
        ################ BOUTTONS GESTION TAB
        self.refresh_button = tk.Button(self.frame_ctrl,text="Refresh SMU",command=self.b_update_SMU)
        self.refresh_button.grid(row=3,column=1)
        
        self.Add_button = tk.Button(self.frame_ctrl,text="Add SMU",command=self.b_add_SMU)
        self.Add_button.grid(row=3,column=2)
        
        self.Add_button = tk.Button(self.frame_ctrl,text="Del SMU",command=self.b_del_SMU)
        self.Add_button.grid(row=3,column=3)
    
        
        ############### Separator #############
        self.separator = ttk.Separator(self.frame_ctrl, orient='horizontal')
        self.separator.grid(row=6, column=0,columnspan=7, sticky="ew", padx=10, pady=10)
        
        ############### FICHIER
        self.Fichier_Chemin_label= tk.Label(self.frame_ctrl,text = "Chemin")
        self.Fichier_Chemin_label.grid(row=7,column=0 )
        
        self.Fichier_Chemin_entry = tk.Entry(self.frame_ctrl, width =50)
        self.Fichier_Chemin_entry.insert(0,"/tmp/")
        self.Fichier_Chemin_entry.grid(row=7,column=1, columnspan=3)
        
        self.Fichier_Nom_label= tk.Label(self.frame_ctrl,text = "Nom")
        self.Fichier_Nom_label.grid(row=8,column=0 )
        
        self.Fichier_Nom_entry = tk.Entry(self.frame_ctrl, width =50)
        self.Fichier_Nom_entry.insert(0,"Mesure_")
        self.Fichier_Nom_entry.grid(row=8,column=1, columnspan=3)
        
        
        ############### Separator ############
        self.separator = ttk.Separator(self.frame_ctrl, orient='horizontal')
        self.separator.grid(row=9, column=0,columnspan=7, sticky="ew", padx=10, pady=10)
        
        ############### VNA 
        self.Fichier_Nom_label= tk.Label(self.frame_ctrl,text = "VNA Adress")
        self.Fichier_Nom_label.grid(row=10,column=0)

        self.VNAAdress_combo = ttk.Combobox(self.frame_ctrl, state='readonly')
        self.VNAAdress_combo.grid(row=10, column = 1)
        
        self.cb_S2Pcal.set(True)
        self.VNAcal_check = tk.Checkbutton(self.frame_ctrl, text = "S2P cal", var= self.cb_S2Pcal, onvalue = 1, offvalue = 0, height=5, width = 20)
        self.VNAcal_check.grid(row=10, column = 2)

        self.VNAraw_check = tk.Checkbutton(self.frame_ctrl, text = "S2P RAW", var= self.cb_S2Praw, onvalue = 1, offvalue = 0, height=5, width = 20)
        self.VNAraw_check.grid(row=10, column = 3)
                
        ############### Separator #############
        self.separator = ttk.Separator(self.frame_ctrl, orient='horizontal')
        self.separator.grid(row=11, column=0,columnspan=7, sticky="ew", padx=10, pady=10)
        
        ############### Mesure / Quit    
        self.DATA_button = tk.Button(self.frame_ctrl,text="Mesure",command=self.b_Mesure)
        self.DATA_button.grid(row=12,column=0)
        
        self.quit_button = tk.Button(self.frame_ctrl,text="Quit",command=self.b_quit)
        self.quit_button.grid(row=12,column=5)
        
        ############### Separator #############
        self.separator = ttk.Separator(self.frame_ctrl, orient='horizontal')
        self.separator.grid(row=13, column=0,columnspan=6, sticky="ew", padx=10, pady=10)

        ############### Indicateur MESURE
        self.Mesure_cours_label= tk.Label(self.frame_ctrl,state= "disabled", text = "MESURE EN COURS",highlightcolor="red",font=('Arial', 14), foreground = 'red')
        self.Mesure_cours_label.grid(row=14,column=0, columnspan = 6)
        
        self.frame_consol = tk.Frame(self)
        self.frame_consol.pack() #grid() #pack()
        
        
               # CIME 
                
        self.consol_text = tk.Text(self.frame_consol, height=10)
        self.consol_text.pack()
        #self.consol_text.insert("end", "<--°°--__--[ CIME Nanotech ]--__--°°-->\n")
        self.consol_text.insert("end", " _____________________________________________________________\n")
        self.consol_text.insert("end", "|                                                             |\n")
        self.consol_text.insert("end", "|   ___                ___                                    |\n")
        self.consol_text.insert("end", "|  /     |   /\  /\   |      |\  |  _       _  |_  _   _ |    |\n")
        self.consol_text.insert("end", "| |      |  /  \/  \  |--    | \ |  _| |-. | | |  |_| |  |-.  |\n")
        self.consol_text.insert("end", "|  \__   | /        \ |___   |  \| |_| | | |_| |_ |_  |_ | |  |\n")
        self.consol_text.insert("end", "|                                                             |\n")
        self.consol_text.insert("end", " -------------------------------------------------------------\n")
        self.consol_text.insert("end", "                   [v1.0]||[10.2022]\n")
        self.consol_text.see("end")
        

################################################ MAIN

if __name__ == "__main__":
    app = VNA_Auto(None)
    app.title('---[ CIME S2P ]---- v1')
    #app.
    app.mainloop()
