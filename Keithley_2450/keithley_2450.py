# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pyvisa as visa
#import time

tension = 1.0

rm = visa.ResourceManager()

#print(rm.list_resources())
#Alim = rm.open_resource("TCPIP::147.171.35.214::inst0::INST")


#Alim = rm.open_resource("TCPIP::147.171.35.214::5025::SOCKET", write_termination = '\n\r',read_termination='\n\r')
#Alim = rm.open_resource("TCPIP::147.171.35.236::5025::SOCKET")#, write_termination = '\n\r',read_termination='\n\r')
#Alim = rm.open_resource("TCPIP::147.171.35.214::inst0::INSTR", write_termination = '\n\r',read_termination='\n\r')


Alim = visa.ResourceManager().open_resource("USB::1510::9296::04435673::0::INSTR", timeout=10000)


Alim.timeout = 5000
print(Alim.query("*IDN?"))
print(Alim.query(":OUTP1:STAT?"))
Alim.write(":SOUR:FUNC:MODE VOLT")
Alim.write(":DISP:CURR:DIG 5")
Alim.write(":TRIG:CONT OFF")


Alim.write(":SOUR:VOLT " + str(tension) +"E-2")

Alim.write(":OUTP1 ON")
courant = Alim.query(":MEAS:CURR?")
Alim.write(":OUTP1 OFF")

print("Courant = " + courant)
print("Resistance = " + str(tension/float(courant)))


Alim.close()


# Alim2 = rm.open_resource("TCPIP::147.171.35.214::inst0::INSTR")
# Alim2.timeout = 5000
# print(Alim2.query("*IDN?"))
# Alim2.close()

