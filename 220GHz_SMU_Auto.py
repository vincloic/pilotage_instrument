#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 15:06:03 2020

@author: vincent
"""

### TODO
# 

import sys
import pyvisa
#import time
#import skrf

#########################################################
#                   Fonction MESURE VNA                 #
#########################################################
def mesure_VNA(tension,courant):
    ###################################### MESURE VNA
    # Mesure en cal
    print("| Mesure Cal ON", end = "...")
    VNA.write(":SENS1:CORR:STATE ON")
    VNA.write("TRS;WFS;OS2P")

    try:
      data_on = VNA.read(encoding='latin9')
    except:
      data_on = 0
      print("> Error read")

    print("[OK]")

    # Mesure en non cal
    print("| Mesure Cal OFF", end="...")
    VNA.write(":SENS1:CORR:STATE OFF")
    VNA.write("TRS;WFS;OS2P")
    try:
      data_off = VNA.read(encoding='latin9')
    except:
      data_off = 0
      print("> Error read")


    #data_off = VNA.read_raw()
    print("[OK]")

    ###################################### SAUVEGARDE FICHIER S2P
    # Enregistrement des data en cal et non cal
    print("Sauvegarde Fichier", end="...")
    data_on = data_on[11:]
    data_off = data_off[11:]


    f_don = open("/tmp/test_on__V-" + str(tension) + "mV__I-" + str(courant) + "mA.s2p", 'w')
    f_don.write(data_on)
    f_don.close()

    f_doff = open("/tmp/test_off__V-" + str(tension) + "mV__I-" + str(courant) + "mA.s2p", 'w')
    f_doff.write(data_off)
    f_doff.close()
    print("[OK]")


#########################################################
#                Fonction Boucle de mesure              #
#########################################################
def boucle_mesure(debut,fin, pas):
    for i in range(debut,fin,pas):
        SMU.write(":SOUR:VOLT " + str(i)  + "E-03")
        SMU.write(":OUTP1 ON")
        courant = float(SMU.query(":MEAS:CURR?"))
        print(" ===============================================================")
        print("| Tension = " + str(i)  + "mV" + " | Courant = " + str(courant) + "mA")
        print(" ---------------------------------------------------------------")
        mesure_VNA(i, courant)
        SMU.write(":OUTP1 OFF")
        



#########################################################
#                          MAIN                         #
#########################################################
   
####################################### CONFIG VISA
try:
    VNA = pyvisa.ResourceManager().open_resource("USB::2907::65488::1741843::0::INSTR", timeout=10000)
except:
    print("> Erreur : VNA non trouvé")
    sys.exit()

try:
    SMU = pyvisa.ResourceManager().open_resource("USB::1510::9296::04435673::0::INSTR", timeout=10000)
except:
    print("> Erreur SMU non trouvé")
    sys.exit()

print(VNA.query("*IDN?"))
print(SMU.query("*IDN?"))

####################################### CONFIG VNA + format des S2P
VNA.write("LANG NATIVE")
VNA.write(":CALC1:FORM:S2P:PORT PORT14")        # Choix du port 1 et 4

VNA.write(":CALC1:PAR:COUN 4")                  # Affiche 4 courbe
VNA.write(":CALC1:PAR1:DEF S11")                # Affiche S11
VNA.write(":CALC1:PAR2:DEF S14")                # Affiche S14
VNA.write(":CALC1:PAR3:DEF S41")                # Affiche S41
VNA.write(":CALC1:PAR4:DEF S44")                # Affiche S44
VNA.write(":DISP:WIND:SPL R2C2")                # affiche 4 plot

VNA.write(":FORM:SNP:FREQ MHZ")                 # demande la frequence en MHz
VNA.write(":FORM:SNP:PAR REIM")                 # demande un reel et imaginaire
VNA.write(":FORM:DATA ASC")                     # en mode ASCII

SMU.write(":SOUR:FUNC:MODE VOLT")               # SMU en mode tension
SMU.write(":DISP:CURR:DIG 5")                   # 5 digit de mesure de courant
SMU.write(":TRIG:CONT OFF")                     # trigger non continue

# Doucle de meusres debut, fin et pas
boucle_mesure(-1200, -400, 100)
boucle_mesure(-400,   400, 10)
boucle_mesure( 400,  1200, 100)



VNA.close()
SMU.close()