#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 15:44:05 2022

@author: vincent
"""

import pyvisa 

rm = pyvisa.ResourceManager()
list_inst = rm.list_resources()
print(list_inst)

for adress in list_inst :
    try :
        print("\n")
        print(str(adress))
        intr = pyvisa.ResourceManager().open_resource(str(adress), timeout=2000)
        print(intr.query("*IDN?"))
        intr.close()
    except :
        print("Erreur Ouvertur intrument  " + str(adress))


intr = pyvisa.ResourceManager().open_resource("TCPIP0::147.171.35.99::inst0::INSTR", timeout=2000)
print(intr.query("*IDN?"))
intr.close()

##intr = pyvisa.ResourceManager().open_resource("USB0::1510::9296::04435673::0::INSTR", timeout=10000)
##print(intr.query("*IDN?"))

##print(intr.query("FORM:DATA?"))
##intr.write(":CALC1:FORM:S2P:PORT PORT14")
##print(intr.query(":CALC1:FORM:S2P:PORT?"))
### Selection de la 1er trace
##
##intr.write(":CALC1:PAR:COUN 4")
###vna.write(":CALC1:PAR1:SEL")
###time.sleep(2)
#### Demande de changer la trace selectioné en S11
##intr.write(":CALC1:PAR1:DEF S11")
##intr.write(":CALC1:PAR2:DEF S14")
##intr.write(":CALC1:PAR3:DEF S41")
##intr.write(":CALC1:PAR4:DEF S44")
##intr.write(":DISP:WIND:SPL R2C2")
print("Fin")
