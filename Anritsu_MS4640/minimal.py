#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 15:06:03 2020

@author: vincent
"""

### TODO
# 

import pyvisa
#import time
#import skrf

sleeptime = 1 # temps de poste entre mesure en (s)
## 20 GHz
#vna = pyvisa.ResourceManager().open_resource("USB::2907::65488::1931956::0::INSTR", timeout=10000)
## 220 GHz
vna = pyvisa.ResourceManager().open_resource("USB::2907::65488::1741843::0::INSTR", timeout=10000)
print(vna.query("*IDN?\n"))

# config du format des s2p
vna.write("LANG NATIVE")
vna.write(":FORM:SNP:FREQ HZ")
vna.write(":FORM:SNP:PAR REIM")


# Mesure en cal
vna.write(":SENS1:CORR:STATE ON")
vna.write("TRS;WFS;OS2P")
data_on = vna.read()

# Mesure en non cal
vna.write(":SENS1:CORR:STATE OFF")
vna.write("TRS;WFS;OS2P")
data_off = vna.read()

# Enregistrement des data en cal et non cal
f_don = open('/tmp/test_on.s2p', 'w')
f_don.write(data_on)
f_don.close()

f_doff = open('/tmp/test_off.s2p', 'w')
f_doff.write(data_off)
f_doff.close()

vna.close()