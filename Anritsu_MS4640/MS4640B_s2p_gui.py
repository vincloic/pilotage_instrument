#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 15:06:03 2020

@author: vincent
"""

### TODO
# 

import pyvisa as visa
import time
import tkinter as tk

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.master.title("MS4642B - S2P")
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        self.tempo_txt = tk.Label()
        self.tempo_txt["text"] = "Temporisation"
        self.tempo_txt.grid(row = 0, column=0)
        
        self.tempo_val = tk.Entry()
        self.tempo_val.grid(row=0,column=1)
        
        self.mesure_btn = tk.Button(self)
        self.mesure_btn["text"] = "Mesure S2P"
        self.mesure_btn["command"] = self.mesure_s2p
        self.mesure_btn.grid(row=1, column=0)

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.grid(row=1, column=1)
   
    def mesure_s2p(self):
        sleeptime = 1 # temps de poste entre mesure en (s)
        
        vna = visa.ResourceManager().open_resource("USB::2907::65488::1931956::0::INSTR")
        print(vna.query("*IDN?\n"))
        
        # Selection de la 1er trace
        vna.write(":CALC1:PAR1:SEL")
        
        # Choix format ASCII
        vna.write(":FORM:DATA ASC\n")
        
        # Recuperation de la liste des frequence
        freq = vna.query(":SENS1:FREQ:DATA?\n")
        freq = freq.split()
        freq.pop(0) 
        #print(freq)
        
        
        ## Demande  type de la trace selectioné en S11
        type_trace = vna.query(":CALC1:PAR:DEF?")
        
        ## Nombre de trace
        nbre_trace = vna.query(":CALC1:PAR:COUN?")
        
        vna.write(":CALC1:PAR:COUN 1")
        #time.sleep(2)
        ## Demande de changer la trace selectioné en S11
        vna.write(":CALC1:PAR:DEF S11")
        time.sleep(sleeptime)
        ## Demande de Re + Im de la trace selectionné
        s11 = vna.query(":CALC1:DATA:SDAT?")
        s11 = s11[int(s11[1]) + 2 : len(s11)]
        s11 = s11.split('\n')
        
        ## Demande de changer la trace selectioné en S12
        vna.write(":CALC1:PAR:DEF S12")
        time.sleep(sleeptime)
        ## Demande de Re + Im de la trace selectionné
        s12 = vna.query(":CALC1:DATA:SDAT?")
        s12 = s12[int(s12[1]) + 2 : len(s12)]
        s12 = s12.split('\n')
        
        ## Demande de changer la trace selectioné en S21
        vna.write(":CALC1:PAR:DEF S21")
        time.sleep(sleeptime)
        ## Demande de Re + Im de la trace selectionné
        s21 = vna.query(":CALC1:DATA:SDAT?")
        s21 = s21[int(s21[1]) + 2 : len(s21)]
        s21 = s21.split('\n')
        
        ## Demande de changer la trace selectioné en S22
        vna.write(":CALC1:PAR:DEF S22")
        time.sleep(sleeptime)
        ## Demande de Re + Im de la trace selectionné
        s22 = vna.query(":CALC1:DATA:SDAT?")
        s22 = s22[int(s22[1]) + 2 : len(s22)]
        s22 = s22.split('\n')
        
        f = open('/tmp/test.s2p', 'w')
        f.write("! -._.-°-> CIME Nanotech <-°°-._.- \n")
        f.write("! " + time.asctime() + "\n")
        f.write("! Freq[Hz] \t S11[re] \t S11[im] \t\t S21[re] \t S21[im] \t\t S12[re] \t S12[im] \t\t S22[re] \t S22[im] \n")
        f.write("#  HZ   S   RI   R     50.00 \n")
        
        for x in range(len(freq)):
        
            f.write(freq[x] + "   "
                   + s11[x].replace(",", "   ") + "   "
                   + s21[x].replace(",", "   ") + "   "
                   + s12[x].replace(",", "   ") + "   "
                   + s22[x].replace(",", "   ")
                   )
            f.write("\n")
        
        f.close()
        
        ## Demande  type de la trace selectioné en S11
        vna.write(":CALC1:PAR:DEF " + type_trace)
        vna.write(":CALC1:PAR:COUN " + nbre_trace )
        
        
        vna.close()
        
        
        
root = tk.Tk()
app = Application(master=root)
app.mainloop()