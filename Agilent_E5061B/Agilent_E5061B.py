#!/usr/bin/python

import time,csv, os
import pygtk
pygtk.require('2.0')
import gtk

class E5061B:

# Liste des instrument
    def Liste_Instrument(donnee=None) :
      try :
        fliste = open("/dev/usbtmc0", "r")
      except :
        md = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO, gtk.BUTTONS_CLOSE, "Erreur pilote")
        run()
        md.destroy()
      
      liste = fliste.readlines()
      fliste.close()
      liste[len(liste)-1] = liste[len(liste)-1].replace("\n", "")
      return liste
      

# Configuration VNA
    def b_config_clicked(self, widget, donnees=None) :
      f_instru = open("/dev/usbtmc" + self.s_instru.get_text(), "r+")
      f_instru.write(":SYST:PRES\n")
      f_instru.write(":SENS1:FREQ:START "+ self.s_freq_start.get_text() + "\n")
      f_instru.write(":SENS1:FREQ:STOP "+ self.s_freq_stop.get_text() + "\n")
      MLOG = self.c_amplitude.get_active()
      PHASE = self.c_phase.get_active()
      if(MLOG and PHASE) :
        f_instru.write(":CALC1:PAR:COUNT 2\n")
        f_instru.write(":CALC1:PAR1:DEF TR\n")
        f_instru.write(":CALC1:SEL:FORM MLOG\n")
        f_instru.write(":CALC1:PAR2:DEF TR\n")
        f_instru.write(":CALC1:PAR2:SEL\n")
        f_instru.write(":CALC1:SEL:FORM PHASE\n")
      elif (MLOG) :
        f_instru.write(":CALC1:PAR1:DEF TR\n")
        f_instru.write(":CALC1:SEL:FORM MLOG\n")        
      elif (PHASE) :
        f_instru.write(":CALC1:PAR1:DEF TR\n")
        f_instru.write(":CALC1:SEL:FORM PHASE\n")  
      else :
        #print " Rien a faire..."

      f_instru.write(":SENS1:BAND "+ self.s_IFBW.get_text() + "\n")
      f_instru.close()
 
# Auto scale
    def b_autoscale_clicked(self, widget, donnees=None) :
      MLOG = self.c_amplitude.get_active()
      PHASE = self.c_phase.get_active()
      f_instru = open("/dev/usbtmc" + self.s_instru.get_text(), "r+")
      if(MLOG and PHASE) :
        f_instru.write(":DISP:WIND1:TRACE1:Y:AUTO\n")
        f_instru.write(":DISP:WIND1:TRACE2:Y:AUTO\n")
      else :
        f_instru.write(":DISP:WIND1:TRACE1:Y:AUTO\n")
      f_instru.close()
      
# Acquisition
    def b_daq_clicked(self, widget, donnees=None) :
      MLOG = self.c_amplitude.get_active()
      PHASE = self.c_phase.get_active()
      f_instru = open("/dev/usbtmc" + self.s_instru.get_text(), "r+")
      if(MLOG and PHASE) :
        f_instru.write(":DISP:WIND1:TRACE1:Y:AUTO\n")
        f_instru.write(":DISP:WIND1:TRACE2:Y:AUTO\n")
      else :
        f_instru.write(":DISP:WIND1:TRACE1:Y:AUTO\n")

      f_instru.write(":FORM:DATA ASC\n")
      f_instru.write(":SENS1:FREQ:DATA?\n")
      self.data_freq = f_instru.readline() + f_instru.readline()
      self.data_freq = self.data_freq.replace("\n","")
      self.data_freq = self.data_freq.split(',')

      if(MLOG and PHASE) :
        f_instru.write(":CALC1:PAR1:SEL\n")
        f_instru.write(":FORM:DATA ASC\n")
        f_instru.write(":CALC1:DATA:FDAT?\n")	
        data = f_instru.readline() + f_instru.readline() + f_instru.readline()
        print len(data)
        f_instru.write(":CALC1:PAR2:SEL\n")
        f_instru.write(":FORM:DATA ASC\n")
        f_instru.write(":CALC1:DATA:FDAT?\n")
        data2 = f_instru.readline() + f_instru.readline() + f_instru.readline()
        data2 = data2.split(',')
        self.data2_final = []
        for x in range(len(data2)) :
          if data2[x] != "+0.00000000000E+000" and data2[x] != "+0.00000000000E+000\n" :
             self.data2_final.append(data2[x])
      else :
        f_instru.write(":CALC1:PAR1:SEL\n")
        f_instru.write(":FORM:DATA ASC\n")
        f_instru.write(":CALC1:DATA:FDAT?\n")
        data = f_instru.readline() + f_instru.readline() + f_instru.readline()

      f_instru.flush()
      f_instru.close()
      data = data.split(',')

      self.data_final = []
      for x in range(len(data)) :
        if data[x] != "+0.00000000000E+000" and data[x] != "+0.00000000000E+000\n" :
           self.data_final.append(data[x])
 
# Sauvegarde en CSV     
    def b_save_clicked(self, widget, donnees=None) :
      MLOG = self.c_amplitude.get_active()
      PHASE = self.c_phase.get_active()
      choix_fichier = gtk.FileChooserDialog(title="Choix fichier sauvegarde...",action=gtk.FILE_CHOOSER_ACTION_SAVE,buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_SAVE,gtk.RESPONSE_OK))

      jour = ""
      for x in range(5) :
        jour += str(time.localtime()[x]) + "_"
      jour += str(time.localtime()[x])
      chemin = os.getenv("HOME") + "/mesure/"
      print chemin
      choix_fichier.set_filename(chemin + "5061B_" + jour + ".csv")
      choix_fichier.set_current_name("5061B_" + jour + ".csv")
      filtre = gtk.FileFilter()
      filtre.set_name("CSV")
      filtre.add_pattern("*.csv")
      choix_fichier.add_filter(filtre)
      filtre = gtk.FileFilter()
      filtre.set_name("All files")
      filtre.add_pattern("*")

      choix_fichier.add_filter(filtre)
      reponse = choix_fichier.run()
      Fichier = choix_fichier.get_filename()
      if reponse == gtk.RESPONSE_OK :
        csv_W = csv.writer(open(Fichier, "wb"),delimiter="\t")
        if(MLOG and PHASE) :
          csv_W.writerow(["Freq","Data", "Data2"])
        else :
          csv_W.writerow(["Freq","Data"])
        for x in range(len(self.data_final)) :
          self.data_freq[x] = self.data_freq[x].replace("\n","")
          self.data_final[x] = self.data_final[x].replace("\n","")
          self.data_freq[x] = self.data_freq[x].replace(".",",")
          self.data_final[x] = self.data_final[x].replace(".",",")
          if(MLOG and PHASE) :
            self.data2_final[x] = self.data2_final[x].replace("\n","")
            self.data2_final[x] = self.data2_final[x].replace(".",",")
            csv_W.writerow([self.data_freq[x],self.data_final[x],self.data2_final[x]])
          else :
            csv_W.writerow([self.data_freq[x],self.data_final[x]])
      choix_fichier.destroy()

# Fermeture App
    def evnmt_delete(self, widget, evenement, donnees=None):
        gtk.main_quit()
        return False
    
# Fermeture App
    def b_quit_clicked(self, widget, donnees=None):
        gtk.main_quit()
        return False


# INITIALAISATION 
    def __init__(self):
        #os.system("cd /etc/usbtmc")
	#os.system("sudo ./usbtmc_load")
	
	# creation d'une nouvelle fenetre
        self.fenetre = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.fenetre.set_title("Agilent E5061B")
        self.fenetre.connect("delete_event", self.evnmt_delete)

        # On fixe la largeur des bordures de la fenetre.
        self.fenetre.set_border_width(10)

	# Contennaire
	self.H_boite = gtk.HBox(False, 50)
	self.H_boite.show()
	self.fenetre.add(self.H_boite)

	# Image
        self.i_cime = gtk.Image()
        self.i_cime.set_from_file("/usr/local/Agilent_E5061B/img/cime.gif")
        self.H_boite.pack_start(self.i_cime, True, True,0)
        self.i_cime.show()

        self.V_boite = gtk.VBox(True, 0)
	self.V_boite.show()
        self.H_boite.pack_start(self.V_boite, True,True,0)
	self.fenetre.add(self.V_boite)

        self.boite_config = gtk.HBox(True, 0)
        self.V_boite.pack_start(self.boite_config, True,True,0)
        self.boite_config.show()        

        self.boite_statut_instru = gtk.HBox(True, 0)
        self.V_boite.pack_start(self.boite_statut_instru, True,True,0)
        self.boite_statut_instru.show() 

        self.boite_freq_start = gtk.HBox(True, 0)
        self.V_boite.pack_start(self.boite_freq_start, True,True,0)
        self.boite_freq_start.show()        

        self.boite_freq_stop = gtk.HBox(True, 0)
        self.V_boite.pack_start(self.boite_freq_stop, True,True,0)
        self.boite_freq_stop.show()
        
        self.boite_IFBW = gtk.HBox(True, 0)
        self.V_boite.pack_start(self.boite_IFBW, True,True,0)
        self.boite_IFBW.show()

        self.boite_check = gtk.HBox(True, 0)
        self.V_boite.pack_start(self.boite_check, True,False,0)
        self.boite_check.show()

        self.boite_boutton = gtk.HBox(True, 0)
        self.V_boite.pack_start(self.boite_boutton, True,False,0)
        self.boite_boutton.show()

        # Choix Instrument
        self.l_instru = gtk.Label("Instrument numero")
        self.boite_config.pack_start(self.l_instru, True, True, 0)
        self.l_instru.show()
        self.s_instru = gtk.Entry(0)
	self.s_instru.set_text("1")
        self.boite_config.pack_start(self.s_instru, True,True,0)
	self.s_instru.show()

        #Liste instrument
        self.liste_instru = gtk.TextView()
        self.liste_instru.set_editable(False)
        buffertext = self.liste_instru.get_buffer()
        buffertext.set_text(str(self.Liste_Instrument()[0]) + str(self.Liste_Instrument()[1]))
        self.boite_statut_instru.pack_start(self.liste_instru, True, True, 0)
        self.liste_instru.show()

	# frequence start
        self.l_freq_start = gtk.Label("Frequence debut")
        self.boite_freq_start.pack_start(self.l_freq_start, True,True,0)
	self.l_freq_start.show()
        self.s_freq_start = gtk.Entry(0)
	self.s_freq_start.set_text("30")
        self.boite_freq_start.pack_start(self.s_freq_start, True,True,0)
	self.s_freq_start.show()

	# frequence stop
        self.l_freq_stop = gtk.Label("Frequence fin")
        self.boite_freq_stop.pack_start(self.l_freq_stop, True,True,0)
	self.l_freq_stop.show()
        self.s_freq_stop = gtk.Entry(0)
	self.s_freq_stop.set_text("30E6")
        self.boite_freq_stop.pack_start(self.s_freq_stop, True,True,0)
	self.s_freq_stop.show()

        # choix IF bandwidth
        self.l_IFBW = gtk.Label("IF Bandwidth")
        self.boite_IFBW.pack_start(self.l_IFBW, True,True,0)
	self.l_IFBW.show()
        self.s_IFBW = gtk.Entry(0)
	self.s_IFBW.set_text("3000")
        self.boite_IFBW.pack_start(self.s_IFBW, True,True,0)
	self.s_IFBW.show()

        # Choix Amplitude et/ou Phase
        self.c_amplitude = gtk.CheckButton("Amplitude")
        self.c_amplitude.set_active(True)
        self.boite_check.pack_start(self.c_amplitude, True, True, 2)
        self.c_amplitude.show()
        self.c_phase = gtk.CheckButton("Phase")
        self.boite_check.pack_start(self.c_phase, True, True, 2)
        self.c_phase.show()

        # Boutton config
        self.b_config = gtk.Button("_Config")
        self.b_config.connect("clicked", self.b_config_clicked, None)
        self.boite_boutton.pack_start(self.b_config, True, True, 0)
        self.b_config.show()
 
        # Boutton auto scale
        self.b_autoscale = gtk.Button("A_uto Scale")
        self.b_autoscale.connect("clicked", self.b_autoscale_clicked, None)
        self.boite_boutton.pack_start(self.b_autoscale, True, True, 0)
        self.b_autoscale.show()

        # Boutton DAQ
        self.b_daq = gtk.Button("_Acquisition")
        self.b_daq.connect("clicked", self.b_daq_clicked, None)
        self.boite_boutton.pack_start(self.b_daq, True, True, 0)
        self.b_daq.show()

        # Boutton save
        self.b_save = gtk.Button("_Sauver")
        self.b_save.connect("clicked", self.b_save_clicked, None)
        self.boite_boutton.pack_start(self.b_save, True, True, 0)
        self.b_save.show()

	# Boutton Quitter
        self.b_quit = gtk.Button("_Quitter")
        self.b_quit.connect_object("clicked", self.b_quit_clicked, None)
        self.boite_boutton.pack_start(self.b_quit, True, True, 0)
        self.b_quit.show()

        # Affichage Fenetre
        self.fenetre.show()

    def boucle(self):
        gtk.main()

# ---------------- MAIN --------------------
if __name__ == "__main__":
    App = E5061B()
    App.boucle()
